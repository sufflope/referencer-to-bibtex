object Versions {
  val grizzled = "1.3.3"
  val kantan   = "0.5.0"
  val logback  = "1.2.3"
  val scopt    = "3.7.1"
}
