lazy val root = Project(id = "referencer-to-bibtex", base = file("."))
  .settings(moduleName := "root")
  .enablePlugins(UnpublishedPlugin)
  .aggregate(app)

lazy val app = project
  .settings(
    assemblyJarName := s"${name.value}-${version.value}.jar",
    buildInfoKeys   := Seq[BuildInfoKey](BuildInfoKey(name), version),
    name            := "referencer-to-bibtex"
  )
  .enablePlugins(BuildInfoPlugin, StrictPlugin, UnpublishedPlugin)
  .settings(
    libraryDependencies ++= Seq(
      "ch.qos.logback"   % "logback-classic" % Versions.logback,
      "com.github.scopt" %% "scopt"          % Versions.scopt,
      "com.nrinaudo"     %% "kantan.xpath"   % Versions.kantan,
      "org.clapper"      %% "grizzled-slf4j" % Versions.grizzled,
    )
  )
