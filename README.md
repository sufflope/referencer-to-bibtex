# referencer-to-bibtex

Integrate your local file reference, from Referencer XML reflib, to your Bibtex bibliography.

## Requirements

You need Java 8 and a version of [sbt](https://www.scala-sbt.org/download.html) able to boostrap the actual version used in this project (`1.2.8`), so most probably `sbt 1.x`.

## Useful commands

### Start sbt REPL

Simply type `sbt`. All subsequent commands assume you are in an `sbt` session.

### Codestyle

Format automatically the code according to `scalafmt` stylesheet using `scalafmtAll`. You can check violations using `scalafmtCheck`.

### Tests

Run the tests, codestyle verifications and generate code coverage reports with `validate`. Code coverage reports will be in `target/scala-2.12/scoverage-report`.

### Build

Run `assembly` to build the jar at `app/target/scala-2.12/referencer-to-bibtex-1.0.0-SNAPSHOT.jar`. Run it (outside sbt REPL) with `java -jar path/to/jar`.
