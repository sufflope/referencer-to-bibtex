package rtb

import kantan.xpath.NodeDecoder
import kantan.xpath.implicits._

final case class Doc(id: String, file: Option[String])

object Doc {

  implicit val docDecoder: NodeDecoder[Doc] =
    NodeDecoder.decoder[String, Option[String], Doc](xp"./key", xp"./relative_filename")(Doc.apply)

}
