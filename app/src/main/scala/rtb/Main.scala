package rtb

import grizzled.slf4j.Logging
import java.nio.file.Files
import kantan.xpath.implicits._
import scala.collection.JavaConverters._

object Main extends App with Logging {

  private val Start = """@[\w]+\{(.*),""".r

  System.setProperty("line.separator", "\r\n")

  for (Args(Some(reflib), Some(in), out) ← Args.parse(args)) {
    for {
      docs ← reflib.evalXPath[Seq[Doc]](xp"/library/doclist/doc")
      duplicates = docs.groupBy(_.id).filter(_._2.size > 1).keys
      _ = if (duplicates.nonEmpty)
        warn(
          s"These keys are duplicated in Referencer reflib : ${duplicates.mkString(", ")}. The first occurence's file will be used."
        )
    } Files.write(
      out.getOrElse(in).toPath,
      Files
        .lines(in.toPath)
        .iterator()
        .asScala
        .toList
        .flatMap {
          case start @ Start(key) ⇒
            start :: docs.collectFirst { case Doc(id, Some(file)) if id == key ⇒ s"\tfile = {:$file:PDF}," }.toList
          case line ⇒ List(line)
        }
        .asJava
    )
  }

}
