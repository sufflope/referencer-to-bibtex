package rtb

import buildinfo.BuildInfo
import java.io.File
import scopt.OptionParser

@SuppressWarnings(Array("org.wartremover.warts.DefaultArguments"))
final case class Args(reflib: Option[File] = None, in: Option[File] = None, out: Option[File] = None)

object Args {

  def parse(args: Array[String]): Option[Args] = parser.parse(args, Args())

  val parser: OptionParser[Args] = new OptionParser[Args](BuildInfo.name) {

    head(BuildInfo.name, BuildInfo.version)

    opt[File]('r', "reflib")
      .valueName("<file>")
      .action { (file, args) ⇒
        args.copy(reflib = Some(file))
      }
      .validate(exists)
      .text("load Referencer reflib from <file>")
      .required()

    opt[File]('i', "in")
      .valueName("<file>")
      .action { (file, args) ⇒
        args.copy(in = Some(file))
      }
      .validate(exists)
      .text("load existing Bibtex from <file>")
      .required()

    opt[File]('o', "out")
      .valueName("<file>")
      .action { (file, args) ⇒
        args.copy(out = Some(file))
      }
      .text("write updated Bibtex to <file> (if omitted, <in> will be overwritten)")
      .optional()

    help("help").abbr("h").text("display help")

    version("version").abbr("v").text("display version number")

    private def exists(file: File) =
      if (file.isFile) success
      else failure(s"Not an existing file: '$file'")

  }

}
